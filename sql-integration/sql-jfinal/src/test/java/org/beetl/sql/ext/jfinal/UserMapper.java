package org.beetl.sql.ext.jfinal;

import org.beetl.sql.mapper.BaseMapper;

public interface  UserMapper extends BaseMapper<JFinalUser> {
}
